/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

/**
 *
 * @author tud08
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1.5);
        Circle circle2 = new Circle(4.5);
        Circle circle3 = new Circle(5.5);
        Rectangle rectangle1 = new Rectangle(2.2, 3.3);
        Rectangle rectangle2 = new Rectangle(5.2, 4.6);
        Sqaure sqaure1 = new Sqaure(10.3);
        Sqaure sqaure2 = new Sqaure(8.6);
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(sqaure1);
        System.out.println(sqaure2);
        
        Shape[] shape = {circle1, circle2, circle3, rectangle1, rectangle2, sqaure1, sqaure2};
        for (int i = 0; i < shape.length; i++){
            System.out.println(shape[i].getName() + " area : " + shape[i].calArea());
        }
    }
}
